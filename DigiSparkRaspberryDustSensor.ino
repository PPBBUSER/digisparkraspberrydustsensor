/*
  Copyright 2016 Piotr Padkowski
  runs on DigiSpark @16MHz with GP2Y1010AUF sensor

   Connections:
   dust sensor led power supply (6)    - +5V via 150ohm resistor and 220µF capacitor (see datasheet)
   dust sensor analog power supply (1) - 5V of DigiSpark
   dust sensor led drive pin (3)       - P2 of DigiSpark
   dust sensor output  (5)             - P4 (analog 2) of DigiSpark
   dust sensor grounds (2,4)           - to ground
   DigiSpark P0 via 330ohms resisor to raspberry RxD
   Raspberry +5V to DigiSpark +5V
   Indicator led is already connected on DigiSpark  
 */
#include <SoftSerial.h>     
#define LED_PIN 2 // dust sensor LED driving pin
#define INPUT_PIN 2 // this is not the same pin as LED_PIN, analog pins have different numbering
#define SERIAL_OUT 0
#define SERIAL_IN_DUMMY 1
#define INDICATOR_LED 1

SoftSerial mySerial(SERIAL_IN_DUMMY, SERIAL_OUT); // RX, TX we are only interested in tx

void setup()  
{
  pinMode(LED_PIN, OUTPUT);
   
  // set the data rate for the SoftwareSerial port
  mySerial.begin(9600);
  mySerial.println("Init");
  pinMode(INDICATOR_LED, OUTPUT);
  analogReference(INTERNAL1V1); 
}


#define MIN_VOLTAGE     600 // in mV, as in datasheet chart


/*
 * to understand this look at dust sensor datasheet - ther are timing diagrams 
 * we are rejecting values lower than what's shown on sensor response plot (600mV)
 * values are converted to µg/(m*m*m)
 */
float measureDust() {
  digitalWrite(LED_PIN,HIGH);
  delayMicroseconds(280);
  int value = analogRead(INPUT_PIN);
  //delayMicroseconds(40); // analogRead should be long enough
  digitalWrite(LED_PIN,LOW);

  float voltage = (1100.0 / 1024.0) * value * 11; // we are using 1.1V reference; 1024 if ADC full scale
 
  delay(10);
 
  if (voltage > MIN_VOLTAGE)
  {
    return (voltage - MIN_VOLTAGE) * 0.2; // we assume linear range...
  }
  return 0;  
}

#define AVE_COUNT 5 // number of values to average 

bool ledVal = false;
void loop() 
{
  int i=0;
  float ave_value = 0;
  // toggle the indicator
  digitalWrite(INDICATOR_LED, ledVal);   
  ledVal=!ledVal;
  
  while (i<AVE_COUNT) {
 
    float value = measureDust();
    if (value > 0) {
      ave_value = ave_value + value;
      i++;
      // toggle the indicator
      digitalWrite(INDICATOR_LED, ledVal); 
      ledVal = !ledVal;
    }
  }
  ave_value = ave_value / AVE_COUNT;
  mySerial.println(ave_value);  
}
